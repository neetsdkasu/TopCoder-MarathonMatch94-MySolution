Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System

Public Module Research

    Public Sub Main()
        Try
            Dim M As Integer = CInt(Console.ReadLine())
            Dim matrix(M - 1) As Integer
            For i As Integer = 0 To UBound(matrix)
                matrix(i) = CInt(Console.ReadLine())
            Next i
            
            Dim seed As Integer = CInt(Environment.GetCommandLineArgs()(1))
            Dim S As Integer = CInt(Math.Sqrt(M))
            Dim cntZeros As Integer = 0
            Dim cntPluss As Integer = 0
            For Each v As Integer In matrix
                If v = 0 Then cntZeros += 1
                If v > 0 Then cntPluss += 1
            Next v
            Dim cntMinus As Integer = M - cntPluss - cntZeros
            Dim crossNonZeros As Integer = 0
            For i As Integer = 0 To S - 1
                If matrix(i * S + i) <> 0 Then crossNonZeros += 1
            Next i
            
            Console.Error.WriteLine( _
                "seed: {0:D8}, S: {1,3:D}, S*S: {2,5:D}, " _
                + "(Z,P,M): ({3,5:D},{4,5:D},{5,5:D}), " _
                + "CrsNZ: {6,3:D}", _
                seed, S, M, cntZeros, cntPluss, cntMinus, crossNonZeros)
            
            Dim ret(S - 1) As Integer
            For i As Integer = 0 To UBound(ret)
                ret(i) = i
            Next i

            Console.WriteLine(ret.Length)
            For Each r As Integer In ret
                Console.WriteLine(r)
            Next r
            Console.Out.Flush()
            
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module