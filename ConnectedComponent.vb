Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer, Integer, Integer)

Public Class ConnectedComponent
    Private Sub shuffle(Of T)(rand As Random, arr() As T)
        For i As Integer = 0 To UBound(arr) - 1
            Dim j As Integer = rand.Next(i, arr.Length)
            Dim tmp As T = arr(i)
            arr(i) = arr(j)
            arr(j) = tmp
        Next i
    End Sub
    Dim S As Integer
    Dim perm() As Integer
    Dim dx() As Integer = {0, 1, 0, -1}
    Dim dy() As Integer = {1, 0, -1, 0}
    Dim tmpX() As Integer, tmpY() As Integer

    Private Function calcScore(matrix() As Integer, perm() As Integer) As Double
        Dim flag(S - 1, S - 1) As Boolean
        Dim maxSum As Double = - S * S * 10# * S
        For y As Integer = 0 To (S - 1)
            For x As Integer = 0 To (S - 1)
                If flag(x, y) Then Continue For
                Dim v As Integer = matrix(perm(y) * S + perm(x))
                flag(x, y) = True
                If v = 0 Then Continue For
                Dim sum  As Double = CDbl(v)
                Dim size As Integer = 1
                Dim indx As Integer = 0
                tmpX(0) = x
                tmpY(0) = y
                Do
                    For i = 0 To 3
                        Dim px As Integer = tmpX(indx) + dx(i)
                        If px < 0 OrElse px >= S Then Continue For
                        Dim py As Integer = tmpY(indx) + dy(i)
                        If py < 0 OrElse py >= S Then Continue For
                        If flag(px, py) Then Continue For
                        flag(px, py) = True
                        Dim va As Integer = matrix(perm(py) * S + perm(px))
                        If va = 0 Then Continue For
                        sum += CDbl(va)
                        tmpX(size) = px
                        tmpY(size) = py
                        size += 1
                    Next i
                    indx += 1
                Loop While indx < size
                sum *= Math.Sqrt(CDbl(size))
                If sum > maxSum Then maxSum = sum
            Next x
        Next y
        calcScore = maxSum
    End Function
    Public Function permute(matrix() As Integer) As Integer()
        Dim rand As New Random(19831983)
        Dim time0 As Integer = Environment.TickCount + 9800
        Dim time1 As Integer
        S = CInt(Math.Sqrt(CDbl(matrix.Length)))
        ReDim perm(S - 1)
        ReDim tmpX(matrix.Length - 1), tmpY(matrix.Length - 1)
        genPerm(matrix)
        ' Console.Error.WriteLine(time0 - Environment.TickCount)
        Dim ret(S - 1) As Integer
        Dim idx(S - 1) As Integer
        For i As Integer = 0 To UBound(perm)
            ret(i) = perm(i)
            idx(i) = i
        Next i
        Dim maxScore As Double = calcScore(matrix, perm)
        Do
            Dim last As Integer = rand.Next(1, S)
            shuffle(rand, idx)
            Dim tmp As Integer = perm(idx(0))
            For i As Integer = 1 To last
                perm(idx(i - 1)) = perm(idx(i))
            Next i
            perm(idx(last)) = tmp
            Dim score As Double = calcScore(matrix, perm)
            If score > maxScore Then
                maxScore = score
                Array.Copy(perm, ret, ret.Length)
            Else
                For i As Integer = last To 1 Step -1
                    perm(idx(i)) = perm(idx(i - 1))
                Next i
                perm(idx(0)) = tmp
            End If
            time1 = Environment.TickCount - time0
        Loop While time1 < 0
        ' Console.Error.WriteLine(maxScore)
        permute = ret
    End Function
    Private Structure SomeData
        Dim Nums As Integer, Nats As Integer
        Dim PSum As Integer, Sum As Integer
        Dim MaxNum As Integer, Index As Integer
    End Structure
    Private Sub genPerm(matrix() As Integer)
        Dim rows(S - 1) As SomeData
        Dim cols(S - 1) As SomeData
        For i As Integer = 0 To S - 1
            rows(i).Index = i
            cols(i).Index = i
            For j As Integer = 0 To S - 1
                Dim v As Integer = matrix(i * S + j)
                If v = 0 Then Continue For
                If rows(i).MaxNum > v OrElse rows(i).MaxNum = 0 Then
                    rows(i).MaxNum = v
                End If
                If cols(j).MaxNum > v OrElse cols(j).MaxNum = 0 Then
                    cols(j).MaxNum = v
                End If
                rows(i).Nums += 1
                rows(i).Sum += v
                cols(j).Nums += 1
                cols(j).Sum += v
                If v < 0 Then Continue For
                rows(i).Nats += 1
                rows(i).PSum += v
                cols(j).Nats += 1
                cols(j).PSum += v
            Next j
        Next i
        Dim ci1 As Integer = 0, ci2 As Integer = 0
        Dim tmpMaxSize As Integer = 0
        Dim tmpMaxSum As Integer = 0
        Dim tmpMaxJoint As Integer = 0
        Dim tmpMaxNats As Integer = 0
        Dim tmpMaxPSum As Integer = 0
        For i As Integer = 0 To S - 2
            For j As Integer = i + 1 To S - 1
                Dim v0 As Integer = matrix(i * S + i)
                Dim v1 As Integer = matrix(i * S + j)
                Dim v2 As Integer = matrix(j * S + i)
                Dim v3 As Integer = matrix(j * S + j)
                If v0 = 0 AndAlso v3 = 0 Then Continue For
                Dim tmpSize As Integer = 0
                If v0 <> 0 Then tmpSize += 1
                If v1 <> 0 Then tmpSize += 1
                If v2 <> 0 Then tmpSize += 1
                If v3 <> 0 Then tmpSize += 1
                If tmpSize = 0 Then Continue For
                If tmpSize < tmpMaxSize Then Continue For
                Dim tmpSum As Integer = v0 + v1 + v2 + v3
                Dim tmpJoint As Integer = 0
                If v0 <> 0 AndAlso v1 <> 0 Then tmpJoint += 1
                If v0 <> 0 AndAlso v2 <> 0 Then tmpJoint += 1
                If v3 <> 0 AndAlso v1 <> 0 Then tmpJoint += 1
                If v3 <> 0 AndAlso v2 <> 0 Then tmpJoint += 1
                Dim tmpNats As Integer = rows(ci1).Nats + cols(ci1).Nats _
                                       + rows(ci2).Nats + cols(ci2).Nats
                Dim tmpPSum As Integer = rows(ci1).PSum + cols(ci1).PSum _
                                       + rows(ci2).PSum + cols(ci2).PSum
                If tmpSize > tmpMaxSize Then
                    ci1 = i: ci2 = j: tmpMaxSize = tmpSize
                    tmpMaxSum = tmpSum: tmpMaxJoint = tmpJoint
                    tmpMaxNats = tmpNats: tmpMaxPSum = tmpPSum
                    Continue For
                End If
                If tmpJoint < tmpMaxJoint Then Continue For
                If tmpJoint > tmpMaxJoint Then
                    ci1 = i: ci2 = j: tmpMaxSize = tmpSize
                    tmpMaxSum = tmpSum: tmpMaxJoint = tmpJoint
                    tmpMaxNats = tmpNats: tmpMaxPSum = tmpPSum
                    Continue For
                End If
                If tmpSum < tmpMaxSum Then Continue For
                If tmpSum > tmpMaxSum Then
                    ci1 = i: ci2 = j: tmpMaxSize = tmpSize
                    tmpMaxSum = tmpSum: tmpMaxJoint = tmpJoint
                    tmpMaxNats = tmpNats: tmpMaxPSum = tmpPSum
                    Continue For
                End If
                If tmpNats < tmpMaxNats Then Continue For
                If tmpNats > tmpMaxNats OrElse tmpPSum > tmpMaxPSum Then
                    ci1 = i: ci2 = j: tmpMaxSize = tmpSize
                    tmpMaxSum = tmpSum: tmpMaxJoint = tmpJoint
                    tmpMaxNats = tmpNats: tmpMaxPSum = tmpPSum
                    Continue For
                End If
            Next j
        Next i
        genPermPls(matrix, rows, cols)
        If tmpMaxSize > 0 Then
            Exit Sub
        End If
        If tmpMaxSize = 0 Then
            genPermEx(matrix, rows, cols)
        End If
    End Sub
    Private Sub genPermEx(matrix() As Integer, rows() As SomeData, cols() As SomeData)
        Array.Sort(rows, Function(e1 As SomeData, e2 As SomeData) As Integer
            If e1.Nats <> e2.Nats Then Return e2.Nats.CompareTo(e1.Nats)
            If e1.PSum <> e2.PSum Then Return e2.PSum.CompareTo(e1.PSum)
            If e1.Sum  <> e2.Sum  Then Return e2.Sum.CompareTo(e1.Sum)
            If e1.Nums <> e2.Nums Then Return e2.Nums.CompareTo(e1.Nums)
            Return e2.MaxNum.CompareTo(e1.MaxNum)
        End Function)
        Array.Sort(cols, Function(e1 As SomeData, e2 As SomeData) As Integer
            If e1.Nats <> e2.Nats Then Return e2.Nats.CompareTo(e1.Nats)
            If e1.PSum <> e2.PSum Then Return e2.PSum.CompareTo(e1.PSum)
            If e1.Sum  <> e2.Sum  Then Return e2.Sum.CompareTo(e1.Sum)
            If e1.Nums <> e2.Nums Then Return e2.Nums.CompareTo(e1.Nums)
            Return e2.MaxNum.CompareTo(e1.MaxNum)
        End Function)
        Dim selRows(S - 1) As Boolean, selCols(S - 1) As Boolean
        Dim tmpRows(S * 2 + 10) As Integer, tmpCols(S * 2 + 10) As Integer
        Dim rT As Integer = S + 1, rB As Integer = rT
        Dim cL As Integer = S + 1, cR As Integer = cL
        Dim ri As Integer = rows(0).Index
        selRows(ri) = True: tmpRows(rT) = ri
        For i As Integer = 1 To S - 1
            Dim rj As Integer = rows(i).Index
            Dim cc As Integer = 0
            Dim cps(S - 1) As Integer, sums(S - 1) As Integer
            For j As Integer = 0 To S - 1
                Dim ck As Integer = cols(j).Index
                If ck = ri OrElse ck = rj Then Continue For
                Dim v0 = matrix(ri * S + ck)
                If v0 = 0 Then Continue For
                Dim v1 = matrix(rj * S + ck)
                If v1 = 0 Then Continue For
                cps(cc) = ck
                sums(ck) = v0 + v1
                cc += 1
            Next j
            If cc > 0 Then
                Array.Sort(cps, Function(e1 As Integer, e2 As Integer) As Integer
                    Return sums(e2).CompareTo(sums(e1))
                End Function)
                rB = rT + 1
                selRows(rj) = True: tmpRows(rB) = rj
                selCols(cps(0)) = True: tmpCols(cL) = cps(0)
                If cc > 1 Then
                    cR = cL + 1
                    selCols(cps(1)) = True: tmpCols(cR) = cps(1)
                End If
                Exit For
            End If
        Next i
        If rT = rB Then
            Dim cj As Integer = cols(0).Index
            If cj = ri Then cj = cols(1).Index
            For i = 0 To S - 1
                If matrix(ri * S + i) > matrix(ri * S + cj) Then cj = i
            Next i
            selCols(cj) = True: tmpCols(cL) = cj
        End If
        
        Dim place(3, S * 2 + 10) As Boolean
        place(0, cL) = matrix(tmpRows(rT) * S + tmpCols(cL)) <> 0 ' top
        place(0, cR) = matrix(tmpRows(rT) * S + tmpCols(cR)) <> 0
        place(1, rT) = matrix(tmpRows(rT) * S + tmpCols(cR)) <> 0 ' right
        place(1, rB) = matrix(tmpRows(rB) * S + tmpCols(cR)) <> 0
        place(2, cL) = matrix(tmpRows(rB) * S + tmpCols(cL)) <> 0 ' bottom
        place(2, cR) = matrix(tmpRows(rB) * S + tmpCols(cR)) <> 0
        place(3, rT) = matrix(tmpRows(rT) * S + tmpCols(cL)) <> 0 ' left
        place(3, rB) = matrix(tmpRows(rB) * S + tmpCols(cL)) <> 0
        
        Do While (cR - cL + 1) + (rB - rT + 1) < S
            Dim seli As Integer = -1
            Dim selp As Integer = 0
            Dim selSum As Integer, selSize As Integer = -1
            For i As Integer = 0 To S - 1
                If selCols(i) OrElse selRows(i) Then Continue For
                If seli < 0 Then seli = i
                Dim pSums(3) As Integer, pSizes(3) As Integer
                For j As Integer = rT To rB
                    Dim v0 As Integer = matrix(tmpRows(j)* S + i)
                    If v0 = 0 Then Continue For
                    If place(1, j) Then
                        pSums(1) += v0: pSizes(1) += 1
                    End If
                    If place(3, j) Then
                        pSums(3) += v0: pSizes(3) += 1
                    End If
                Next j
                For j As Integer = cL To cR
                    Dim v1 As Integer = matrix(i * S + tmpCols(j))
                    If v1 = 0 Then Continue For
                    If place(0, j) Then
                        pSums(0) += v1: pSizes(0) += 1
                    End If
                    If place(2, j) Then
                        pSums(2) += v1: pSizes(2) += 1
                    End If
                Next j
                For j As Integer = 0 To 3
                    If pSums(j) > selSum _
                            OrElse (pSums(j) = selSum _
                                    AndAlso pSizes(j) > selSize) Then
                        selp = j
                        seli = i
                        selSum = pSums(j)
                        selSize = pSizes(j)
                    End If
                Next j
            Next i
            Select Case selp
            Case 0
                rT -= 1
                selRows(seli) = True: tmpRows(rT) = seli
                For i As Integer = cL To cR
                    If place(0, i) OrElse place(0, i - 1) Then
                        place(0, i) = matrix(seli * S + tmpCols(i)) <> 0
                        If Not place(0, i) Then Continue For
                        If place(0, i - 1) Then Continue For
                        For j As Integer = i - 1 To cL Step -1
                            place(0, j) = matrix(seli * S + tmpCols(j)) <> 0
                            If Not place(0, j) Then Exit For
                        Next j
                    End If
                Next i
            Case 1
                cR += 1
                selCols(seli) = True: tmpCols(cR) = seli
                For i As Integer = rT To rB
                    If place(1, i) OrElse place(1, i - 1) Then
                        place(1, i) = matrix(tmpRows(i) * S + seli) <> 0
                        If Not place(1, i) Then Continue For
                        If place(1, i - 1) Then Continue For
                        For j As Integer = i - 1 To rT Step -1
                            place(1, j) = matrix(tmpRows(j) * S + seli) <> 0
                            If Not place(1, j) Then Exit For
                        Next j
                    End If
                Next i
            Case 2
                rB += 1
                selRows(seli) = True: tmpRows(rB) = seli
                For i As Integer = cL To cR
                    If place(2, i) OrElse place(2, i - 1) Then
                        place(2, i) = matrix(seli * S + tmpCols(i)) <> 0
                        If Not place(2, i) Then Continue For
                        If place(2, i - 1) Then Continue For
                        For j As Integer = i - 1 To cL Step -1
                            place(2, j) = matrix(seli * S + tmpCols(j)) <> 0
                            If Not place(2, j) Then Exit For
                        Next j
                    End If
                Next i
            Case Else
                cL -= 1
                selCols(seli) = True: tmpCols(cL) = seli
                For i As Integer = rT To rB
                    If place(3, i) OrElse place(3, i - 1) Then
                        place(3, i) = matrix(tmpRows(i) * S + seli) <> 0
                        If Not place(3, i) Then Continue For
                        If place(3, i - 1) Then Continue For
                        For j As Integer = i - 1 To rT Step -1
                            place(3, j) = matrix(tmpRows(j) * S + seli) <> 0
                            If Not place(3, j) Then Exit For
                        Next j
                    End If
                Next i
            End Select
        Loop
        Dim ret(S - 1) As Integer
        ' cols ++ rows
        For i As Integer = cL To cR
            perm(i - cL) = tmpCols(i)
        Next i
        For i As Integer = rT To rB
            perm(i - rT + (cR - cL + 1)) = tmpRows(i)
        Next i
        Dim maxScore As Double = calcScore(matrix, perm)
        Array.Copy(perm, ret, ret.Length)
        ' rows ++ cols
        For i As Integer = rT To rB
            perm(i - rT) = tmpRows(i)
        Next i
        For i As Integer = cL To cR
            perm(i - cL + (rB - rT + 1)) = tmpCols(i)
        Next i
        Dim score As Double = calcScore(matrix, perm)
        If score > maxScore Then
            maxScore = score
            Array.Copy(perm, ret, ret.Length)
        End If
        ' cols ++ reverse rows
        For i As Integer = cL To cR
            perm(i - cL) = tmpCols(i)
        Next i
        For i As Integer = rB To rT Step -1
            perm(i - rT + (cR - cL + 1)) = tmpRows(i)
        Next i
        score = calcScore(matrix, perm)
        If score > maxScore Then
            maxScore = score
            Array.Copy(perm, ret, ret.Length)
        End If
        ' reverse cols ++ rows
        For i As Integer = cR To cL Step -1
            perm(i - cL) = tmpCols(i)
        Next i
        For i As Integer = rT To rB
            perm(i - rT + (cR - cL + 1)) = tmpRows(i)
        Next i
        score = calcScore(matrix, perm)
        If score > maxScore Then
            maxScore = score
            Array.Copy(perm, ret, ret.Length)
        End If
        ' dest
        Array.Copy(ret, perm, perm.Length)
    End Sub
    
    Private Sub show(Of T)(ti As String, p() As T, b As Integer, e As Integer)
        Dim sss As String = ti + ": "
        For i As Integer = b To e
            sss += p(i).ToString() + ","
        Next i
        Console.Error.WriteLine(sss)
    End Sub
    
    Private Sub genPermPls(matrix() As Integer, rows() As SomeData, cols() As SomeData)
        Dim seli As Integer = 0
        Dim selScore As Double = 0#
        For i As Integer = 0 To S - 1
            If matrix(i * S + i) = 0 Then Continue For
            Dim tmpSize As Integer = rows(i).Nats + cols(i).Nats
            Dim tmpSum As Integer = rows(i).PSum + cols(i).PSum
            Dim score As Double = CDbl(tmpSum) * Math.Sqrt(CDbl(tmpSize))
            If score > selScore Then
                seli = i
                selScore = score
            End If
        Next i
        Dim tmpPerm(S * 2 + 10) As Integer
        Dim lp As Integer = S + 1, rp As Integer = lp
        Dim lselp As Integer = lp, rselp As Integer = lp
        Dim sels(S - 1) As Boolean
        Dim targH As New List(Of Integer)()
        Dim targV As New List(Of Integer)()
        Dim targB As New List(Of Integer)()
        Dim total As Integer = matrix(seli * S + seli)
        sels(seli) = True
        tmpPerm(lp) = seli
        ' show("first", tmpPerm, lp, rp)
        Dim cmpH As Comparison(Of Tp) = Function(e1 As Tp, e2 As Tp) As Integer
            If e1.Item4 <> e2.Item4 Then Return e2.Item4.CompareTo(e1.Item4)
            If e1.Item3 <> e2.Item3 Then Return e1.Item3.CompareTo(e2.Item3)
            Dim i As Integer = e1.Item2, j As Integer = e2.Item2
            If rows(i).Nats <> rows(j).Nats THen Return rows(j).Nats.CompareTo(rows(i).Nats)
            If rows(i).PSum <> rows(j).PSum THen Return rows(j).PSum.CompareTo(rows(i).PSum)
            If rows(i).Sum  <> rows(j).Sum  THen Return rows(j).Sum.CompareTo(rows(i).Sum)
            Return rows(j).Nums.CompareTo(rows(i).Nums)
        End Function
        Dim cmpV As Comparison(Of Tp) = Function(e1 As Tp, e2 As Tp) As Integer
            If e1.Item4 <> e2.Item4 Then Return e2.Item4.CompareTo(e1.Item4)
            If e1.Item3 <> e2.Item3 Then Return e1.Item3.CompareTo(e2.Item3)
            Dim i As Integer = e1.Item2, j As Integer = e2.Item2
            If cols(i).Nats <> cols(j).Nats THen Return cols(j).Nats.CompareTo(cols(i).Nats)
            If cols(i).PSum <> cols(j).PSum THen Return cols(j).PSum.CompareTo(cols(i).PSum)
            If cols(i).Sum  <> cols(j).Sum  THen Return cols(j).Sum.CompareTo(cols(i).Sum)
            Return cols(j).Nums.CompareTo(cols(i).Nums)
        End Function
        Dim cmbV As New List(Of Tp)()
        Dim cmbH As New List(Of Tp)()
        For i As Integer = 0 To S - 1
            Dim v0 As Integer = matrix(i * S + seli)
            If v0 > 0 Then
                rows(i).Nats -= 1
                rows(i).PSum -= v0
                rows(i).Nums -= 1
                rows(i).Sum -= v0
                targV.Add(i)
            End If
            Dim v1 As Integer = matrix(seli * S + i)
            If v1 > 0 Then
                cols(i).Nats -= 1
                cols(i).PSum -= v1
                cols(i).Nums -= 1
                cols(i).Sum -= v1
                targH.Add(i)
            End If
            If v0 > 0 AndAlso v1 > 0 Then
                targB.Add(i)
            End If
        Next i        
        For ei As Integer = 0 To targB.Count - 1
            Dim i As Integer = targB(ei)
            For ej As Integer = ei + 1 To targV.Count - 1
                Dim j As Integer = targV(ej)
                If i = j Then Continue For
                Dim v1 As Integer = matrix(j * S + i)
                If v1 <> 0 Then
                    cmbV.Add(New Tp(i, j, v1, 1))
                End If
            Next ej
        Next ei
        If cmbV.Count = 0 Then
            For ei As Integer = 0 To targV.Count - 2
                Dim i As Integer = targV(ei)
                For ej As Integer = ei + 1 To targV.Count - 1
                    Dim j As Integer = targV(ej)
                    Dim v0 As Integer = matrix(i * S + j)
                    If v0 <> 0 Then
                        cmbV.Add(New Tp(j, i, v0, 0))
                    End If
                    Dim v1 As Integer = matrix(j * S + i)
                    If v1 <> 0 Then
                        cmbV.Add(New Tp(i, j, v1, 0))
                    End If
                Next ej
            Next ei
        End If
        Dim bth As Integer = 0, cnn As Integer = 0
        If cmbV.Count > 0 Then
            cmbV.Sort(cmpH)
            sels(cmbV(0).Item1) = True
            sels(cmbV(0).Item2) = True
            rp += 1
            tmpPerm(rp) = cmbV(0).Item1
            If cmbV(0).Item4 = 1 Then
                bth = 1
            Else
                cnn = 1
            End If
        End If
        ' show("cmbV", tmpPerm, lp, rp)
        If bth <> 1 Then
            For ei As Integer = 0 To targB.Count - 1
                Dim i As Integer = targB(ei)
                If sels(i) Then Continue For
                For ej As Integer = ei + 1 To targH.Count - 1
                    Dim j As Integer = targH(ej)
                    If sels(j) Then Continue For
                    If i = j Then Continue For
                    Dim v0 As Integer = matrix(i * S + j)
                    If v0 <> 0 Then
                        cmbH.Add(New Tp(i, j, v0, 1))
                    End If
                Next ej
            Next ei
        End If
        If cmbH.Count = 0 Then
            For ei As Integer = 0 To targH.Count - 2
                Dim i As Integer = targH(ei)
                If sels(i) Then Continue For
                For ej As Integer = ei + 1 To targH.Count - 1
                    Dim j As Integer = targH(ej)
                    If sels(j) Then Continue For
                    Dim v0 As Integer = matrix(i * S + j)
                    If v0 <> 0 Then
                        cmbH.Add(New Tp(i, j, v0, 0))
                    End If
                    Dim v1 As Integer = matrix(j * S + i)
                    If v1 <> 0 Then
                        cmbH.Add(New Tp(j, i, v1, 0))
                    End If
                Next ej
            Next ei
        End If
        If cmbH.Count > 0 Then
            cmbH.Sort(cmpH)
            sels(cmbH(0).Item1) = True
            sels(cmbH(0).Item2) = True
            lp -= 1
            tmpPerm(lp) = cmbH(0).Item1
            If cmbH(0).Item4 = 1 Then
                bth = 2
            Else
                cnn += 10
            End If
        End If
        ' show("cmbH", tmpPerm, lp, rp)
        If bth > 0 OrElse cnn <> 11 Then
            For Each e As Integer In targB
                If sels(e) Then Continue For
                sels(e) = True
                If bth = 1 OrElse (bth = 0 AndAlso cnn <> 10) Then
                    lp -= 1
                    tmpPerm(lp) = e
                Else
                    rp += 1
                    tmpPerm(rp) = e
                End If
                total += matrix(e * S + seli)
            Next e
        End If
        ' show("Both", tmpPerm, lp, rp)
        For Each e As Integer In targV
            If sels(e) Then Continue For
            sels(e) = True
            lp -= 1
            tmpPerm(lp) = e
            total += matrix(e * S + seli)
        Next e
        ' show("targV", tmpPerm, lp, rp)
        For Each e As Integer In targH
            If sels(e) Then Continue For
            sels(e) = True
            rp += 1
            tmpPerm(rp) = e
            total += matrix(seli * S + e)
        Next e
        ' show("targH", tmpPerm, lp, rp)
        If cmbH.Count > 0 Then
            ' show("cmbH", tmpPerm, lp, rp)
            sels(cmbH(0).Item2) = True
            lp -= 1
            tmpPerm(lp) = cmbH(0).Item2
            total += matrix(seli * S + cmbH(0).Item2)
            lselp -= 1
        Else
            lselp = lp
        End If
        ' show("cmbH2", tmpPerm, lp, rp)
        If cmbV.Count > 0 Then
            sels(cmbV(0).Item2) = True
            rp += 1
            tmpPerm(rp) = cmbV(0).Item2
            total += matrix(cmbV(0).Item2 * S + seli)
            rselp += 1
        Else
            rselp = rp
        End If
        ' show("cmbV2", tmpPerm, lp, rp)
        
        Dim cmpHV As Comparison(Of Integer) = Function (e1 As Integer, e2 As Integer) As Integer
            If rows(e1).Nats <> cols(e2).Nats Then Return cols(e2).Nats.CompareTo(rows(e1).Nats)
            If rows(e1).PSum <> cols(e2).PSum Then Return cols(e2).PSum.CompareTo(rows(e1).PSum)
            If rows(e1).Sum  <> cols(e2).Sum  Then Return cols(e2).Sum.CompareTo(rows(e1).Sum)
            Return cols(e2).Nums.CompareTo(rows(e1).Nums)
        End Function
        
        ' show("sels", sels, 0, S-1)
                
        Dim minus As New SortedSet(Of Integer)()
        Do While rp - lp + 1 < S
            Dim bseli As Integer = seli
            Dim lef As Boolean
            If cmpHV(tmpPerm(lselp), tmpPerm(rselp)) < 0 Then
                seli = tmpPerm(rselp)
            Else
                seli = tmpPerm(lselp)
                lef = True
            End If
            If seli = bseli Then
                For i As Integer = 0 To S - 1
                    If sels(i) Then Continue For
                    seli = i
                    Exit For
                Next i
            End If
            targH.Clear()
            targV.Clear()
            minus.Clear()
            For i As Integer = 0 To S - 1
                If sels(i) Then Continue For
                If lef Then
                    Dim v0 As Integer = matrix(i * S + seli)
                    If v0 > 0 Then
                        rows(i).Nats -= 1
                        rows(i).PSum -= v0
                        rows(i).Nums -= 1
                        rows(i).Sum -= v0
                        targV.Add(i)
                    ElseIf v0 < 0 Then
                        minus.Add(i)
                    End If
                Else 
                    Dim v1 As Integer = matrix(seli * S + i)
                    If v1 > 0 Then
                        cols(i).Nats -= 1
                        cols(i).PSum -= v1
                        cols(i).Nums -= 1
                        cols(i).Sum -= v1
                        targH.Add(i)
                    ElseIf v1 < 0 Then
                        minus.Add(i)
                    End If
                End If
            Next i
            If targV.Count = 0 AndAlso targH.Count = 0 Then
                If minus.Count = 0 Then Exit Do
                Dim mVal As Integer = -10, mi As Integer = 0
                For Each i As Integer In minus
                    If lef Then
                        Dim v0 As Integer = matrix(seli * S + i)
                        If v0 > mVal Then
                            mVal = v0
                            mi = i
                        End If
                    Else
                        Dim v1 As Integer = matrix(i * S + seli)
                        If v1 > mVal Then
                            mVal = v1
                            mi = i
                        End If
                    End If
                Next i
                If mVal = -10 Then Exit Do
                Dim sc1 As Double = CDbl(total) * Math.Sqrt(rp - lp + 1)
                Dim sc2 As Double = CDbl(total + mVal) * Math.Sqrt(rp - lp + 2)
                If sc2 > sc1 Then
                    If lef Then
                        lp -= 1
                        tmpPerm(lp) = mi
                    Else
                        rp += 1
                        tmpPerm(rp) = mi
                    End If
                    sels(mi) = True
                    ' show("sels-loop", sels, 0, S-1)
                    total += mVal
                Else
                    Exit Do
                End If
                Continue Do
            End If
            If lef Then
                For Each e As Integer In targV
                    If sels(e) Then Continue For
                    sels(e) = True
                    lp -= 1
                    tmpPerm(lp) = e
                    total += matrix(e * S + seli)
                Next e
                lselp -= 1
            Else
                For Each e As Integer In targV
                    If sels(e) Then Continue For
                    sels(e) = True
                    rp += 1
                    tmpPerm(rp) = e
                    total += matrix(e * S + seli)
                Next e
                rselp += 1
            End If
            ' show("loop", tmpPerm, lp, rp)
        Loop
        ' show("sels-after", sels, 0, S-1)
        ' Console.Error.WriteLine("rp:{0}, lp:{1}, diff:{2}, S:{3}", rp, lp, rp-lp+1,S)
        For i As Integer = 0 To S - 1
            If sels(i) Then Continue For
            rp += 1
            tmpPerm(rp) = i
        Next i
        For i As Integer = 0 To S - 1
            perm(i) = tmpPerm(lp + i)
        Next i
        ' show("last", perm, 0, S-1)
    End Sub
End Class